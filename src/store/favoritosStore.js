const FavoritStore = {
    data: {
      favorits: [],
    },
    methods: {
      addFavorite(name) {
        FavoritStore.data.favorits.push(name);
      },
      deleteFavorite(name) {
        FavoritStore.data.favorits.splice(name);
      }
    }
  };
  
  export default FavoritStore;